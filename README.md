About: This is a simple script used to change the collation of MySQL database from any collation to "utf8_unicode_ci". Make sure to take the backup of concerned database before attempting this.

Author: Aman Hanjrah

License: GPL v2.0

Webite: http://techlinux.net

Contact: aman.hanjrah@techlinux.net