#!/bin/bash

# Author: Aman Hanjrah
# Date: 12 March, 2015
# License: GPL v2.0
#
# Instructions: Change the value of "MYSQL_USER", "MYSQL_DB" and "MYSQL_PASS" to read the database information for which you need to change the collation for.
# You can place it anywhere on the server and it will work out of the box.
#
# Start
function init() {
MYSQL_USER="db_user"
MYSQL_DB="db_name"
MYSQL_PASS="db_pass"
}

function get_tbl_list() {
	mysql -u $MYSQL_USER -p$MYSQL_PASS $MYSQL_DB -e "SELECT TABLE_NAME FROM information_schema.TABLES where TABLE_SCHEMA = '$MYSQL_DB';" > db.txt
	sed -i '/TABLE_NAME/d' db.txt
}

function chng_col() {
	for name in `cat db.txt`; do
		mysql -u $MYSQL_USER -p$MYSQL_PASS $MYSQL_DB -e "ALTER TABLE $name CONVERT TO CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci';"
	done
}

function main() {
	init
	get_tbl_list
	if [[ -f db.txt ]]; then
		chng_col
	else
		echo "Changing collation failed because db.txt could not be found!"
	fi
}

main
# End
